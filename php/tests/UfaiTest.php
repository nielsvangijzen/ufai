<?php

use PHPUnit\Framework\TestCase;
use Ufai\Gore;

final class UfaiTest extends TestCase {
    public function testRot13Works() {
        $rot13 = new Gore();

        $s = $rot13->terrgvatf_sebz_ng_avryfinatvwmra();
        $this->assertEquals('greetings_from_at_nielsvangijzen', $s);
    }

    public function testToupperWorks() {
        $toupper = new Gore();

        $s = $toupper->greetings_from_at_nielsvangijzen();
        $this->assertEquals('GREETINGS_FROM_AT_NIELSVANGIJZEN', $s);
    }

    public function testReverseWorks() {
        $rev = new Gore();

        $s = $rev->armwvtanifyrva_gn_zbes_ftavgrret();
        $this->assertEquals('terrgvatf_sebz_ng_avryfinatvwmra', $s);
    }
}