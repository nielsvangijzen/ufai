#!/usr/bin/env node

const Gore = new Proxy(
  {},
  {
    get: (target, methodName) =>
      new Proxy(
        {},
        { get: (target, text) => String.prototype[methodName].call(text) },
      ),
  },
)

const { toUpperCase, toLowerCase } = Gore

console.log(toUpperCase.foobar)
console.log(toLowerCase.FOOBAR)
