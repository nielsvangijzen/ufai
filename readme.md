# UFAI

[![pipeline status](https://gitlab.com/nielsvangijzen/ufai/badges/master/pipeline.svg)](https://gitlab.com/nielsvangijzen/ufai/commits/master)

Using functions as input (UFAI) is a new state of the art design pattern that'll take the industry by storm.
This repo is dedicated to provide examples for this pattern in as many languages as possible!

Feel free to contribute!
